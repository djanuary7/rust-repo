fn main() {
    let number = 7;

    if number < 5 {
        println!("Condition was true");
    } else {
        println!("Condition was false");
    }

    if number != 0{
        println!("number was something other than zero");
    }

    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    println!("The result is {result}");

    // let mut num = 3;

    // while num != 0 {
    //     println!("{num}...");

    //     num -= 1;
    // }

    // println!("LIFTOFF!!!");

    let a = [10, 20, 30, 40, 50];
    // let mut index = 0;

    // while index < 5 {
    //     println!("the value is: {}", a[index]);

    //     index += 1;
    // }
    for element in a {
        println!("the value is: {element}");
    }

    for countdown in (1..4).rev(){
        println!("{countdown}...");
    }
    println!("LIFTOFF!!!");
}